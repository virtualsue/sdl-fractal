package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	const (
		width, height = 1000.0, 750.0
		maxIter       = 100
		cX, cY        = -0.79, 0.15
	)
	//img := image.NewNRGBA(image.Rect(0, 0, width, height))
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()
	title := fmt.Sprintf("Julia Set at %.4f, %.4f", cX, cY)
	window, err := sdl.CreateWindow(title,
		sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED,
		width, height,
		sdl.WINDOW_RESIZABLE)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()
	rend, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)

	for x := 0; x < width; x++ {
		thisx := float64(x)
		var tmp, zx, zy float64
		var c1, c2, c3 uint8
		for y := 0.0; y < height; y++ {
			zx = 1.5 * (thisx - width/2) / (0.5 * width)
			zy = (y - height/2) / (0.5 * height)
			i := maxIter
			for zx*zx+zy*zy < 4.0 && i > 0 {
				tmp = zx*zx - zy*zy + cX
				zy = 2.0*zx*zy + cY
				zx = tmp
				i--
			}
			switch {
			case i == 0:
				// This value is in the set
				c1 = 0
				c2 = 0
				c3 = 0
			case i < 10:
				// blue
				c1 = 185
				c2 = 150
				c3 = 210
			case i >= 10 && i < 25:
				// red
				c1 = 235
				c2 = 60
				c3 = 60
			case i >= 25 && i < 45:
				// green
				c1 = 25
				c2 = 240
				c3 = 70
			case i >= 45 && i < 75:
				// yellow
				c1 = 220
				c2 = 240
				c3 = 110
			default:
				c1 = 60
				c2 = 110
				c3 = 225
			}
			rend.SetDrawColor(c1, c2, c3, 255)
			rend.DrawPoint(int32(thisx), int32(y))
		}
	}
	rend.Present()
	running := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.MouseButtonEvent:
				if t.State == 1 {
					fmt.Println("Mouse button press: ", t.X, t.Y)
					//					show_msgbox(t.X, t.Y)
				}
			case *sdl.QuitEvent:
				fmt.Println("Done")
				running = false
				break
			default:
				//fmt.Printf("Some event\n")
			}
		}
	}
}
