package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"strconv"
)

func show_msgbox(x, y int32) {
	button := []sdl.MessageBoxButtonData{
		{sdl.MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 0, "OK"},
	}
	messageboxdata := sdl.MessageBoxData{
		sdl.MESSAGEBOX_INFORMATION,
		nil,
		"Image Coordinates: " + strconv.Itoa(int(x)) + "," + strconv.Itoa(int(y)),
		"Press OK to dismiss",
		int32(len(button)),
		button,
		nil, // colourscheme implemented on X11, android
	}
	if _, err := sdl.ShowMessageBox(&messageboxdata); err != nil {
		fmt.Println("error displaying message box")
		return
	}
}
