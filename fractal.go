package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"log"
	"math/cmplx"
)

const (
	maxEsc = 1000
	width  = 1600
	height = 1056
)

func mandelbrot(c complex128) (complex128, int) {
	var z = c
	for i := 0; i < maxEsc; i++ {
		z = z*z + c
		if cmplx.Abs(z) > 2 {
			return z, i
		}
	}
	return 0, 0
}

func main() {
	var wid float64 = 3
	var xcentre, ycentre float64 = -1, 0

	hwidth := width / 2
	hheight := height / 2
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("Mandelbrot Set",
		sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED,
		width, height,
		sdl.WINDOW_RESIZABLE)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	rend, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		log.Fatalf("Failed to create renderer: %s\n", err)
	}
	defer rend.Destroy()

	var c1, c2, c3 uint8

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			ca := float64(x-hwidth)/float64(width)*wid + xcentre
			cb := float64(y-hheight)/float64(width)*1*wid + ycentre
			res, i := mandelbrot(complex(ca, cb))
			// Sort out colours & draw point
			switch {
			case res == 0:
				// This value is in the set
				c1 = 0
				c2 = 0
				c3 = 0
			case i < 1:
				// turquoise
				c1 = 33
				c2 = 196
				c3 = 237
			case i >= 1 && i < 3:
				// red
				c1 = 240
				c2 = 40
				c3 = 55
			case i >= 3 && i < 6:
				// green
				c1 = 77
				c2 = 224
				c3 = 69
			case i >= 6 && i < 9:
				// blue
				c1 = 43
				c2 = 62
				c3 = 206
			case i >= 9 && i < 15:
				// yellow
				c1 = 229
				c2 = 237
				c3 = 7
			case i >= 15 && i < 25:
				// vanilla
				c1 = 231
				c2 = 239
				c3 = 174
			case i >= 25 && i < 55:
				// red
				c1 = 240
				c2 = 30
				c3 = 240
			default:
				c1 = 0
				c2 = 128
				c3 = 0
			}
			rend.SetDrawColor(c1, c2, c3, 255)
			rend.DrawPoint(int32(x), int32(y))
		}
	}
	rend.Present()

	running := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.MouseButtonEvent:
				if t.State == 1 {
					fmt.Println("Mouse button press: ", t.X, t.Y)
					show_msgbox(t.X, t.Y)
				}
			case *sdl.QuitEvent:
				fmt.Println("Done")
				running = false
				break
			default:
				//fmt.Printf("Some event\n")
			}
		}
	}
}
